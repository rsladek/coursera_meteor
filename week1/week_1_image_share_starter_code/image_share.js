if (Meteor.isClient){
	var img_data = [
		{
			img_src: "1.jpg",
			img_alt: "first pic"
		},
		{
			img_src: "2.jpg",
			img_alt: "second pic"
		},
		{
			img_src: "3.jpg",
			img_alt: "third pic"
		},
	];

	Template.images.helpers({images: img_data});
}

if (Meteor.isServer){

}
