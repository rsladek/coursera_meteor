Websites = new Mongo.Collection("websites");

Websites.allow({
    // we need to be able to update images for ratings.
    update: function (userId, doc) {
        if (Meteor.user()) {// they are logged in
            return true;
        } else {// user not logged in - do not let them update  (vote) the website.
            return false;
        }
    },
    insert: function (userId, doc) {
        if (Meteor.user()) {// they are logged in
            return true;
        }
        else {// user not logged in
            return false;
        }
    },
    remove: function (userId, doc) {
        if (Meteor.user()) {
            return true;
        } else {
            return false;
        }
    }
});

Posts = new Mongo.Collection("posts");
Posts.allow({
    // we need to be able to update posts for ratings.
    update: function (userId, doc) {
        if (Meteor.user()) {// they are logged in
            return true;
        } else {// user not logged in - do not let them update posts.
            return false;
        }
    },
    insert: function (userId, doc) {
        if (Meteor.user()) {// they are logged in
            return true;
        }
        else {// user not logged in
            return false;
        }
    },
    remove: function (userId, doc) {
        if (Meteor.user()) {
            return true;
        } else {
            return false;
        }
    }
});

SiteInfos = new Mongo.Collection("siteinfos")
SiteInfos.allow({
    // we need to be able to update SiteInfos for ratings.
    update: function (userId, doc) {
        if (Meteor.user()) {// they are logged in
            return true;
        } else {// user not logged in - do not let them update siteinfo.
            return false;
        }
    },
    insert: function (userId, doc) {
        if (Meteor.user()) {// they are logged in
            return true;
        }
        else {// user not logged in
            return false;
        }
    },
    remove: function (userId, doc) {
        if (Meteor.user()) {
            return true;
        } else {
            return false;
        }
    }
});
