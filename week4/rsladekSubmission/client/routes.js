/// routing

Router.configure({
    layoutTemplate: 'ApplicationLayout'
});

Router.route('/', function () {
    this.render('navbar', {
        to: "navbar"
    });
    this.render('website_list', {
        to: "main"
    });
}, {
    name: "home",
});

Router.route('/addwebsite', function () {
    this.render('navbar', {
        to: "navbar"
    });
    this.render('website_form', {
        to: "main",
    });
});

Router.route('/website/:_id', function () {
    this.render('navbar', {
        to: "navbar"
    });
    this.render('website_details', {
        to: "main",
        data: function () {
            Meteor.subscribe("getWebsiteDetails", this.params._id);
            return Websites.findOne({_id: this.params._id});
        }
    });
});
