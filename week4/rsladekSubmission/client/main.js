//////
// helpers
//////

function isLogged() {
    if (Meteor.user()) {
        return true;
    } else {
        return false;
    }
}

function getUserName(userId) {
    var user = Meteor.users.findOne({_id: userId});
    if (user) {
        return user.username;
    }
    else {
        return "anon";
    }
}

function cleanUpSiteInfo() {
    if (isLogged()) {
        var userId = Meteor.userId();
        Meteor.call('siteInfo.remove', userId);
        Meteor.call('siteInfo.create', userId);
    }
}

function isValidUrl(url) {
    var regexQuery = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$";
    var urlregex = new RegExp(regexQuery, "i");
    return urlregex.test(url);
}

/////
// template helpers
/////

Template.search.events({
    'keyup .js-set-website-filter': function (event) {
        Session.set("searchValue", event.currentTarget.value);
    }
});

Template.website_list.rendered = function () {
    $("#items").toggle('slow');
    Session.set("searchValue", undefined);
}

// helper function that returns all available websites
Template.website_list.helpers({
    websites: function () {
        Meteor.subscribe("search", Session.get("searchValue"));

        if (Session.get("searchValue")) {// they set a filter!
            return Websites.find({}, {sort: [["score", "desc"]]});
        }
        else {
            return Websites.find({}, {sort: {votes: -1}});
        }
    }
});

Template.website_form.rendered = function () {
    cleanUpSiteInfo();
    $("#getInfo").toggle('slow');
    $("#tryUrl").focus();
};

Template.website_form.helpers({
    loggedUser: function () {
        return isLogged();
    },
    siteInfos: function () {
        var userId = Meteor.userId();
        Meteor.subscribe("latestSiteInfo", userId);
        return SiteInfos.find({}, {sort: {createdOn: -1}, limit: 1});
    }
});

Template.navbar.helpers({
    loggedUser: function () {
        return isLogged();
    },
});

Template.website_item.helpers({
    getUserName: function (userId) {
        return getUserName(userId);
    },
    getVotes: function () {
        return this.votes ? this.votes : 0;
    }
});

Template.post_item.helpers({
    getUserName: function (userId) {
        return getUserName(userId);
    },
});

Template.website_post_form.helpers({
    loggedUser: function () {
        return isLogged();
    },
});

Template.website_details.helpers({
    getUserName: function (userId) {
        return getUserName(userId);
    },
    posts: function () {
        Meteor.subscribe("posts", this._id);
        return Posts.find({websiteId: this._id}, {sort: {createdOn: -1}});
    },
    hasPosts: function () {
        Meteor.subscribe("posts", this._id);
        return Posts.find({websiteId: this._id}, {sort: {createdOn: -1}}).count();
    },
    getVotes: function () {
        return this.votes ? this.votes : 0;
    }
});

/////
// template events
/////

Template.website_item.events({
    "click .js-upvote": function (event) {
        // example of how you can access the id for the website in the database
        // (this is the data context for the template)
        var website_id = this._id;
        // put the code in here to add a vote to a website!
        Meteor.call("website.voting", {websiteId: website_id, vote: 1})
        return false;// prevent the button from reloading the page
    },
    "click .js-downvote": function (event) {

        // example of how you can access the id for the website in the database
        // (this is the data context for the template)
        var website_id = this._id;
        Meteor.call("website.voting", {websiteId: website_id, vote: -1});

        // put the code in here to remove a vote from a website!

        return false;// prevent the button from reloading the page
    }
});

Template.website_form.events({
    "keyup .js-try-url": function (event) {
        var url = event.currentTarget.value;
        if (url == '') { //Check to see if there is any text entered
            //If there is no text within the input, disable the button
            $('#getInfoButton').attr('disabled', 'disabled');
        } else {
            // Domain name regular expression
            if (isValidUrl(url)) {
                // Domain looks OK
                $('#getInfoButton').removeAttr('disabled');
            } else {
                // Domain is NOT OK
                $('#getInfoButton').attr('disabled', 'disabled');
            }
        }
    },
    "click .js-try-get-site-info": function (event) {
        cleanUpSiteInfo();
        var tryUrl = $("#tryUrl").val();
        Meteor.call('httpGetSiteInfo', {url: tryUrl, userId: Meteor.userId()});
    },
    "submit .js-save-website-form": function (event) {
        //  put your website saving code in here!
        Meteor.call('website.insert', {
            title: event.target.title.value,
            url: event.target.url.value,
            description: event.target.description.value,
            user: Meteor.user()._id
        });

        $("#website_form").toggle('hide');
        Router.go("/");
        return false;// stop the form submit from reloading the page
    }
});
Template.website_post_form.events({
    "click .js-toggle-website-post-form": function (event) {
        $("#website_post_form").toggle('slow');
    },
    "submit .js-save-website-post-form": function (event) {
        Meteor.call('post.insert', {
            title: event.target.post_title.value,
            content: event.target.post_content.value,
            user: Meteor.user()._id,
            websiteId: this._id
        });
        $("#website_post_form").toggle('hide');
        return false;// stop the form submit from reloading the page
    }
});

Template.website_post_form.rendered = function () {
    $('#post_title').focus();
};
