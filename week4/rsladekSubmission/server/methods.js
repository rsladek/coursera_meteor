Meteor.methods({
    'website.insert'({title, url, description, user}) {
        check(title, String);
        check(url, String);
        check(description, String);
        check(user, String);

        Websites.insert({
            title: title,
            url: url,
            description: description,
            createdOn: new Date(),
            votes: 0,
            user: user
        });
    },

    'website.voting'({websiteId, vote}){
        check(vote, Number);
        check(websiteId, String);
        Websites.update({_id: websiteId}, {$inc: {votes: vote}})
    },
    'post.insert'({title, content, user, websiteId}){
        check(title, String);
        check(content, String);
        check(user, String);
        check(websiteId, String);

        Posts.insert({
            title: title,
            content: content,
            createdOn: new Date(),
            user: user,
            websiteId: websiteId
        });
    },
    'siteInfo.remove'(userId){
        SiteInfos.remove({userId: userId});
    },
    'siteInfo.create'(userId){
        SiteInfos.insert({userId: userId});
    },
    'httpGetSiteInfo': function ({url, userId}) {
        check(url, String);
        check(userId, String);
        this.unblock();
        try {
            var result = HTTP.call("GET", url);
            var status = result.statusCode;
            if (status === 200) {
                $ = cheerio.load(result.content);
                var result = {
                    "userId": userId,
                    "url": url,
                    "status": status,
                    "title": $("title").text(),
                    "createdOn": new Date(),
                    "description": $("meta[name='description']").attr("content")
                };
                SiteInfos.insert(result);
            }
        } catch (e) {
            // Got a network error, time-out or HTTP error in the 400 or 500 range.
            console.log("Error: " + e);
        }
    },
});