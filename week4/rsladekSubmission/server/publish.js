Meteor.publish('getWebsiteDetails', function (websiteId) {
    check(websiteId, String);
    return Websites.find({_id: websiteId});
});

Meteor.publish('posts', function (websiteId) {
    check(websiteId, String);
    return Posts.find({websiteId: websiteId}, {sort: {createdOn: -1}});
});

// If `searchValue` is not provided, we publish all known messages. If it is
// provided, we publish only messages that match the given search value.
Meteor.publish("search", function (searchValue) {
    if (!searchValue) {
        return Websites.find({});
    }
    return Websites.find(
        {$text: {$search: searchValue}},
        {
            // `fields` is where we can add MongoDB projections. Here we're causing
            // each document published to include a property named `score`, which
            // contains the document's search rank, a numerical value, with more
            // relevant documents having a higher score.
            fields: {
                score: {$meta: "textScore"}
            },
            // This indicates that we wish the publication to be sorted by the
            // `score` property specified in the projection fields above.
            sort: {
                score: {$meta: "textScore"}
            }
        }
    );
});

Meteor.publish("latestSiteInfo", function (userId) {
    //return SiteInfos.find({userId: userId}, {sort: {createdOn: -1}, limit: 1});
    return SiteInfos.find({}, {sort: {createdOn: -1}, limit: 1});
});