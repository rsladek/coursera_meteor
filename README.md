# Coursera course #
## Title: Introduction to Meteor.js Development##
## Author: Dr Matthew Yee-King##

Earn certification on July 13, 2016

[<img src="https://d3njjcbhbojbot.cloudfront.net/web/images/signature/template-cert-small.png">](https://www.coursera.org/account/accomplishments/certificate/AJC36ASEBGUV)

Link [https://www.coursera.org/learn/meteor-development/home/welcome](https://www.coursera.org/learn/meteor-development/home/welcome)

May 16, 2016 - Jun 20, 2016

University of London & Goldsmiths, University of London

### Week 1###

By the end of this module, you will be able to:

* Install [Meteor](https://www.meteor.com/) tools
* [Create and run](http://guide.meteor.com/#what-is-meteor) a Meteor application
* Edit a template
* Define a template helper
* Define template event listeners

### Week 2###

By the end of the module you will be able to:

* Create Mongo Collections - `var Images = new Mongo.Collection('images')`
* Use Mongo find and insert operations - `Images.find().count()`, `Images.insert({img_src:"img_1.jpg", img_alt:"Image 1"}) `
* Control a Bootstrap modal from Meteor
* Use third party Meteor packages to add functionality

### Week 3###

By the end of this module, you will be able to:

* Add user authentication to your Meteor app
* Use Mongo filters
* Use the Meteor reactive session variable
* Implement an infinite scroll

### Week 4###

By the end of the module, you will be able to:

* Perform basic security testing on your app
* Implement basic data security features
* Organise Meteor application code
* Implement multiple routes using iron:router

## Final assignment ##

### Instructions ###

Make a social website aggregator using Meteor. A social website aggregator allows users to share, discuss and rank interesting webpages they have found.

You are provided with some starter code. Please download the following zip file before starting your assignment.

### Review criterialess ###

Everyone in the course must assess the work of 3 of their peers in order to gain a grade for this assessment. Some people choose to mark even more work, as this exposes them to more ideas and coding techniques.

### Instructionsless ###

You are to make a website aggregator application. The purpose of the application is to allow users to share, discuss and rate pages they find on the internet. You are provided with some starter code which displays a simple list of websites. You can gain up to 70% by implementing the following features:

* Users can register and login.
* Users can post new websites if they are logged in. Websites posted by users should have an URL and a description.
* Users can up and down vote webpages by clicking a plus or a minus button.
* Websites should be listed with the most up voted site first.
* The listing page shows when the website was added and how many up and down votes it has.
* Users can move to a detail page for a website (using routing).
* On the detail page, users can post comments about a webpage, and they are displayed below the description of the webpage.
* You can gain a further 30% by implementing one or more of the following features:

### Challenge 1: Automatic information ###

Can you use the HTTP package for Meteor to pull in information about the posted web links automatically, so the user does not need to enter anything other than the URL?

### Challenge 2: Search function ###

Implement a search function that allows the user to search within the listed sites for key words

### Challenge 3: Website recommender ###

Can you recommend websites to users based on things they have up voted and commented on? E.g. if I up vote a site entitled ‘Tofu recipes’, it would recommend other sites with ‘tofu’ and ‘recipe’ in their titles or descriptions
